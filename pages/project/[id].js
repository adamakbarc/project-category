import { useRouter } from 'next/router'
import Head from 'next/head'
import useSWR from 'swr'

const client = (...args) => fetch(...args).then((res) => res.json())

export default function ProjectDetails() {
  const router = useRouter()
  const { id } = router.query
  const { data, error } = useSWR('/api/projects', client)

  if (error) return <h1>Failed to load project data</h1>
  if (!data) return <h1>Loading..</h1>

  let projectDetail = {}

  for (const project of data) {
    if (project.id == id) {
        projectDetail = project
    }
  }

  return (
    <>
      <Head>
        <title>{projectDetail.title} | Our Project</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <main className="p-4">
        <h1 className="font-bold text-3xl mb-5">{projectDetail.title}</h1>
        <p>{projectDetail.description}</p>
      </main>
    </>
  )
}