// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default function handler(req, res) {
  res.status(200).json([
    {
      id: 1,
      category: 'mangrove',
      title: 'Mangrove 1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis est vulputate ante accumsan faucibus. Nullam non vulputate dolor, vel finibus ligula. Ut sit amet varius dui. Pellentesque urna purus, tincidunt ut tellus sit amet, varius feugiat nisl. Praesent viverra, quam ac aliquet tristique, nibh quam sagittis massa, eget pharetra purus urna eu enim. Aenean erat velit, auctor sit amet libero sed, dapibus varius nunc. Curabitur sed dolor ut metus consequat auctor. Nunc pretium sit amet nisi pretium mollis. Vestibulum vel lorem venenatis, sollicitudin purus eget, consequat ipsum. Praesent malesuada dui in ex imperdiet, eget venenatis nisi vestibulum. Vivamus id sagittis risus. Nullam risus est, pretium eu efficitur porttitor, vehicula non ante. Quisque at ex enim. Nulla magna justo, volutpat blandit pellentesque ut, consequat nec tortor. Sed et elit est.'
    },
    {
      id: 2,
      category: 'mangrove',
      title: 'Mangrove 2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis est vulputate ante accumsan faucibus. Nullam non vulputate dolor, vel finibus ligula. Ut sit amet varius dui. Pellentesque urna purus, tincidunt ut tellus sit amet, varius feugiat nisl. Praesent viverra, quam ac aliquet tristique, nibh quam sagittis massa, eget pharetra purus urna eu enim. Aenean erat velit, auctor sit amet libero sed, dapibus varius nunc. Curabitur sed dolor ut metus consequat auctor. Nunc pretium sit amet nisi pretium mollis. Vestibulum vel lorem venenatis, sollicitudin purus eget, consequat ipsum. Praesent malesuada dui in ex imperdiet, eget venenatis nisi vestibulum. Vivamus id sagittis risus. Nullam risus est, pretium eu efficitur porttitor, vehicula non ante. Quisque at ex enim. Nulla magna justo, volutpat blandit pellentesque ut, consequat nec tortor. Sed et elit est.'
    },
    {
      id: 3,
      category: 'forestry',
      title: 'Forestry 1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis est vulputate ante accumsan faucibus. Nullam non vulputate dolor, vel finibus ligula. Ut sit amet varius dui. Pellentesque urna purus, tincidunt ut tellus sit amet, varius feugiat nisl. Praesent viverra, quam ac aliquet tristique, nibh quam sagittis massa, eget pharetra purus urna eu enim. Aenean erat velit, auctor sit amet libero sed, dapibus varius nunc. Curabitur sed dolor ut metus consequat auctor. Nunc pretium sit amet nisi pretium mollis. Vestibulum vel lorem venenatis, sollicitudin purus eget, consequat ipsum. Praesent malesuada dui in ex imperdiet, eget venenatis nisi vestibulum. Vivamus id sagittis risus. Nullam risus est, pretium eu efficitur porttitor, vehicula non ante. Quisque at ex enim. Nulla magna justo, volutpat blandit pellentesque ut, consequat nec tortor. Sed et elit est.'
    },
    {
      id: 4,
      category: 'forestry',
      title: 'Forestry 2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis est vulputate ante accumsan faucibus. Nullam non vulputate dolor, vel finibus ligula. Ut sit amet varius dui. Pellentesque urna purus, tincidunt ut tellus sit amet, varius feugiat nisl. Praesent viverra, quam ac aliquet tristique, nibh quam sagittis massa, eget pharetra purus urna eu enim. Aenean erat velit, auctor sit amet libero sed, dapibus varius nunc. Curabitur sed dolor ut metus consequat auctor. Nunc pretium sit amet nisi pretium mollis. Vestibulum vel lorem venenatis, sollicitudin purus eget, consequat ipsum. Praesent malesuada dui in ex imperdiet, eget venenatis nisi vestibulum. Vivamus id sagittis risus. Nullam risus est, pretium eu efficitur porttitor, vehicula non ante. Quisque at ex enim. Nulla magna justo, volutpat blandit pellentesque ut, consequat nec tortor. Sed et elit est.'
    },
    {
      id: 5,
      category: 'forestry',
      title: 'Forestry 3',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis est vulputate ante accumsan faucibus. Nullam non vulputate dolor, vel finibus ligula. Ut sit amet varius dui. Pellentesque urna purus, tincidunt ut tellus sit amet, varius feugiat nisl. Praesent viverra, quam ac aliquet tristique, nibh quam sagittis massa, eget pharetra purus urna eu enim. Aenean erat velit, auctor sit amet libero sed, dapibus varius nunc. Curabitur sed dolor ut metus consequat auctor. Nunc pretium sit amet nisi pretium mollis. Vestibulum vel lorem venenatis, sollicitudin purus eget, consequat ipsum. Praesent malesuada dui in ex imperdiet, eget venenatis nisi vestibulum. Vivamus id sagittis risus. Nullam risus est, pretium eu efficitur porttitor, vehicula non ante. Quisque at ex enim. Nulla magna justo, volutpat blandit pellentesque ut, consequat nec tortor. Sed et elit est.'
    },
  ])
}
