import Head from 'next/head'
import ProjectCard from '../components/project-card.js'
import useSWR from 'swr'

const client = (...args) => fetch(...args).then((res) => res.json())

export default function Home() {
  const { data, error } = useSWR('/api/projects', client)

  if (error) return <h1>Failed to load project data</h1>
  if (!data) return <h1>Loading..</h1>

  let projectByCategory = []

  for (const project of data) {
    if (projectByCategory[project.category] === undefined) {
      projectByCategory[project.category] = [project]
    } else {
      projectByCategory[project.category].push(project)
    }
  }

  return (
    <>
      <Head>
        <title>Our Project</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <main className="p-4">
        <h1 className="font-bold text-3xl mb-5">Our Projects</h1>

        <div className="flex flex-col items-start">
          {Object.entries(projectByCategory).map(([category, projects], index) => {
            return (
              <div key={index} className="pb-4">
                <h2 className="mb-4 capitalize font-bold text-2xl">{category}</h2>
                <div className="grid grid-cols-2 gap-4">
                  {projects.map((project, index2) => (
                    <ProjectCard key={index2} projectId={project.id} title={project.title} description={project.description} />
                  ))}
                </div>
              </div>
            )
          })}
        </div>

      </main>
    </>
  )
}
