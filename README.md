Project assignment for Fairatmos front-end vacancy
## Getting Started

First, run the development server:

```bash
npm install
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

There are 3 routes presented:

- `/`  home route to see categorized project list.
- `/project/{id}` detail route to see project detail.
- `/api/projects` simulate API server which return list of projects

Built with NextJS and TailwindCSS