import Link from "next/link";

export default function ProjectCard(props) {
  return (
    <div className="flex flex-col shadow-md p-4 mr-4 border border-neutral-400 rounded-md">
      <h3 className="font-bold text-lg mb-2">{props.title}</h3>
      <p>{props.description.substring(0, 180)}{props.description.length >= 200 && '...'}</p>
      <Link 
        className="flex flex-col items-center border border-neutral-600 bg-slate-200 hover:bg-slate-300 rounded-md p-2 my-2 drop-shadow-sm font-bold"
        href={`/project/${props.projectId}`}
      >
          See More
      </Link>
    </div>
  )
}